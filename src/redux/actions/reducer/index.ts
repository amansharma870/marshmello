import {combineReducers, createStore} from 'redux';
import {DiaryReducer} from './DiaryReducer';
export const rootReducer = combineReducers({
  diaryReducer: DiaryReducer,
});

export default createStore(rootReducer);