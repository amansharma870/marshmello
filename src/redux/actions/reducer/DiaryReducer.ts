import {DiaryActionTypes} from '../../actions/DiaryActions';

interface DiaryReducerState {
  diaryItems: any[];
}

const initialState: DiaryReducerState = {
  diaryItems: [
    {
      id: 0,
      subject: 'my subject 1',
      date: 'March 28, 2020 - Sat',
      time: '3:12 am',
      description:
        'Today was one of my best day of life.Today was one of my best day of life. Today was one of my best day of life.  ',
    },
    {
      id: 1,
      subject: 'my subject 2',
      date: 'March 28, 2020 - Sat',
      time: '3:12 am',
      description:
        'Today was one of my best day of life.Today was one of my best day of life. Today was one of my best day of life.  ',
    },
  ],
};

export const DiaryReducer = (
  action,
  state = initialState,
): DiaryReducerState => {
  switch (action.type) {
    case DiaryActionTypes.ADD_DIARY_ITEM: {
      const diaryItems = state.diaryItems;
      diaryItems.push(action.payload);
      return {...state, diaryItems};
    }
  
    
    default: {
      return state;
    }
  }
};