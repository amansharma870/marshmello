import { Ditem } from './../../components/modals/diaryItem';

export enum DiaryActionTypes {
  ADD_DIARY_ITEM = 'Add diary Item',
  UPDATE_DIARY_ITEM = 'update diary Item',
  DELETE_DIARY_ITEM = 'update diary Item',
}

export class DiaryActions {
  static AddDiaryItemAction = (diaryItem:Ditem) => {
    return {
      type: DiaryActionTypes.ADD_DIARY_ITEM,
        payload: diaryItem,
    };
  };
  static UpdateDiaryItemAction = (diaryItem: Ditem) => {
    return {
      type: DiaryActionTypes.UPDATE_DIARY_ITEM,
      payload: diaryItem,
    };
  };

  static DeleteDiaryItemAction = (id: number) => {
    return {
      type: DiaryActionTypes.DELETE_DIARY_ITEM,
      payload: id,
    };
  };
}