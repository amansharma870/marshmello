import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import { PrimaryTheme } from './Theme'

export const Typography : any = {
    title:{
    fontSize:wp('8%'),
    color:'green',
    fontWeight:"500",
    fontFamily:'Muli',
    marginBottom: wp("5%"),
    },
    heading:{
    fontSize:wp('8%'),
    color:PrimaryTheme.$TEXT_COLOR_900,
    fontWeight:"bold"
    },
    subheading:{
    fontSize:wp('5%'),
    color:PrimaryTheme.$TEXT_COLOR_700,
    fontFamily:'Muli',
    fontWeight:"500"
    },
    paragraph:{
    fontSize:wp('4%'),
    color:PrimaryTheme.$TEXT_COLOR_300,
    fontFamily:'Muli',
    },
    lighText:{
    fontSize:wp('3.2%'),
    color:PrimaryTheme.$TEXT_COLOR_500,
    fontFamily:'Muli',
    },
    errorText:{
    fontSize:wp('3.2%'),
    color:PrimaryTheme.$Error_Color,
    fontFamily:'Muli',
    }
    }
    export const Spacing = {
        tiny: {
          marginLeft: wp('0.25%'),
          marginRight: wp('0.25%'),
          marginTop: hp('0.25%'),
          marginBottom: hp('0.25%'),
        },
        small: {
          marginLeft: wp('0.5%'),
          marginRight: wp('0.5%'),
          marginTop: hp('0.5%'),
          marginBottom: hp('0.5%'),
        },
        regular: {
          marginLeft: wp('1%'),
          marginRight: wp('1%'),
          marginTop: hp('1%'),
          marginBottom: hp('1%'),
        },
        large: {
          marginLeft: wp('1.5%'),
          marginRight: wp('1.5%'),
          marginTop: hp('1.5%'),
          marginBottom: hp('1.5%'),
        },
        extraLarge: {
          marginLeft: wp('2%'),
          marginRight: wp('2%'),
          marginTop: hp('2%'),
          marginBottom: hp('2%'),
        },
      };
      
export const pComponentStyle = {
    textinput:{
    borderWidth:1,
    width: wp('90%'),
    marginBottom:3,
    fontSize:16,
    borderColor:"#000",
    backgroundColor: "#fff",
    },
    }
    
    export const lComponentStyle = {
    textinput:{
    ...pComponentStyle.textinput, width: wp('100%'),
    },
    }