import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Login from '../../screens/login'
import { Dashboard } from '../../screens/Dashboard';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import PICKER from '../../screens/Picker';
import ViewDiaryItem from '../../screens/ViewDiaryItem'
import { View } from 'react-native';
import Icon from '../../components/Icon'
const Stack=createStackNavigator();
export enum ScreenName{
    LOGIN='Login',
    DASHBOARD='Dashboard',
    PICKER='Select Profile',
    VIEW_DIARY_ITEM='view diary item'
}

export const authStack=()=>{
    return(
        <Stack.Navigator initialRouteName={ScreenName.LOGIN}>
          <Stack.Screen name={ScreenName.LOGIN} component={Login}/>
        </Stack.Navigator>
    )
}
export const appStack=()=>{
    return(
        <Stack.Navigator
            screenOptions={{
            headerStyle:{
                backgroundColor:'#3fbea5',
                height:hp('8%'),
            },
            headerLeft:()=>(
                <View style={{margin:12}}>
                 <Icon name={'menu'} color={'#fff'}/>
              </View>
              )
           ,
            headerRight:()=>(
              <View style={{margin:12}}>
                 <Icon name={'home'} color={'#fff'}/>
              </View>
              )
           ,
            headerTitleStyle:{
                color:'#fff'
            },
            title:'ReactApp',
            
        }} initialRouteName={ScreenName.DASHBOARD}>
            
          <Stack.Screen name={ScreenName.DASHBOARD} component={Dashboard}/>
          <Stack.Screen options={{
              headerTintColor:'#fff',
            headerStyle:{
                backgroundColor:'red',
                height:hp('8%'),
            },
            headerLeft:()=>(
                <View style={{margin:12}}>
                 <Icon name={'menu'} color={'#fff'}/>
              </View>
              )
           ,
            headerRight:()=>(
              <View style={{margin:12}}>
                 <Icon name={'home'} color={'#fff'}/>
              </View>
              )
           ,
            headerTitleStyle:{
                color:'#fff'
            },
            title:'View',
            
        }} name={ScreenName.VIEW_DIARY_ITEM} component={ViewDiaryItem}/>
        </Stack.Navigator>
    )
}
export const Picker=()=>{
    return(
        <Stack.Navigator
        screenOptions={{
        headerStyle:{
            backgroundColor:'yellow',
            height:hp('8%')
        },
        headerTitleStyle:{
            color:'#fff'
        },
        title:'PICKER',
        
    }} initialRouteName={ScreenName.PICKER}>

          <Stack.Screen name={ScreenName.PICKER} component={PICKER}/>
        </Stack.Navigator>
    )
}