import  { NavigationContainer } from '@react-navigation/native'
import { appStack, authStack,Picker } from './Routes'
import React from 'react'

export const Navigators =()=>{
    return(
        <NavigationContainer>
            {appStack()}
        </NavigationContainer>
    )
}