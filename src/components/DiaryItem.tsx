import React from 'react'
import {SafeAreaView,Image, StyleSheet, _Image} from 'react-native'
import CustomText from './CustomText';
import {heightPercentageToDP as hp , widthPercentageToDP as wp} from 'react-native-responsive-screen'

import { View } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { utils } from '../utils/utils';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Spacing } from '../styles/Global';
import { Ditem } from './modals/diaryItem';
export interface Porps{
     diaryItem:Ditem;
     onPress:any;
}
const DiaryItem=(props)=>{
    return(
        <SafeAreaView style={{marginTop:Spacing.extraLarge.marginTop,marginBottom:Spacing.large.marginBottom,marginLeft:Spacing.large.marginLeft}}>
            
            <TouchableOpacity  onPress={props.onPress} activeOpacity={0.8} style={{marginBottom:hp('2%')}}>
                <CustomText style={styles.DateNTime}>{props.diaryItem.date}</CustomText>
                <CustomText style={styles.DateNTime}>{props.diaryItem.time}</CustomText>
            </TouchableOpacity>
            <TouchableOpacity  onPress={props.onPress} activeOpacity={0.8} style={{flexDirection:'row'}}>
                <View style={{width:widthPercentageToDP('64%'),marginRight:'2%'}}>
                    <CustomText style={{color:'#000', fontWeight:'bold'}}>{props.diaryItem.subject}</CustomText>
                    <CustomText>{props.diaryItem.description}</CustomText>
                </View>
                <View>
                    <Image style={{width:wp('30%'),height:hp('15%'),borderRadius:5}} source={utils.Image.Image_diary}/>
                </View>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles=StyleSheet.create({
    DateNTime:{
        color:'grey',
        fontWeight:'bold'
    }
})
DiaryItem.defaultProps={};
export default DiaryItem;