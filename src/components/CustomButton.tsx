import * as React from 'react';
import {} from 'react-native'
import {PrimaryTheme} from '../styles/Theme'
import Icons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
  StyleSheet,
} from 'react-native';
import CustomText from './CustomText';
import IF from './If';

export interface Props {
  title: string;
  disabled?: boolean;
  buttonStyle?: ViewStyle | ViewStyle[];
  textStyle?: TextStyle | TextStyle[];
  onPress: any;
  IconName?:string;
  IconColor?:string;
  useIcon?:boolean;
  IconSize?:number
}

const CustomButton = (props: Props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        props.disabled ? {...styles.buttonStyle,backgroundColor:'#000'}
       : styles.buttonStyle, props.buttonStyle,
      props.buttonStyle,
    
    ]}
      disabled={props.disabled}>
      <IF show={props.useIcon}>
      <Icons 
         name={props.IconName} 
         color={props.IconColor} 
         size={props.IconSize}
      />
      </IF>
      <CustomText style={[styles.textStyle, props.textStyle]}>{props.title}</CustomText>
    </TouchableOpacity>
  );
};
CustomButton.defaultProps = {
  disabled: false,
  IconSize:30,
  IconColor:'#fff',
};
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor:PrimaryTheme.$BACK_GRIOND,
    borderRadius: 5,
    width:wp('90%'),
    borderWidth:1,
    height:hp('7%'),
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center'
  },
  textStyle: {
    color: '#fff',
    letterSpacing:3,
    fontSize:18
  },
});
export default CustomButton;

