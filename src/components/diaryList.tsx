import React from 'react'
import {FlatList, View} from 'react-native'
import { Spacing } from '../styles/Global'
import DiaryItem from './DiaryItem'
import { Ditem } from './modals/diaryItem'

export interface Props{
     diaryItem:Ditem[]
     onPress:any;
}
const DiaryList=(props:Props)=>{
    return(
        <>
            <FlatList 
             style={{marginTop:Spacing.extraLarge.marginTop}}
             showsVerticalScrollIndicator={false}
             keyExtractor={(item,index)=>
                 index.toString()
             }
             data={props.diaryItem}
             renderItem={data=>{
                 return <DiaryItem onPress={()=>props.onPress(data.item)} diaryItem={data.item}/>
             }} />
        </>
    )
}
DiaryList.defaultProps={};
export default DiaryList;