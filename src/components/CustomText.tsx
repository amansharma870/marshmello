import React from 'react'
import { SafeAreaView, Text,StyleSheet, TextStyle, TextInput } from 'react-native'
import {Typography} from '../styles/Global'
interface Props{
    children:any;
    style?: TextStyle | TextStyle[];
    numberOfLines?:any;
}

const CustomText=(props)=>{
    return(
       <SafeAreaView>
           <Text numberOfLines={props.numberOfLines} allowFontScaling={false} style={[styles.style,props.style]}>
             {props.children}
           </Text>
       </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    style:{
        ...Typography.paragraph
    },
})
CustomText.defaultProps={
    numberOfLines:3,
}
export default CustomText;