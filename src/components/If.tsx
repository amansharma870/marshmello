import React from 'react'
import {View} from 'react-native'

export interface Props{
    show:boolean;
    children:any;
}

const IF=(props:Props)=>{
    return <>
        {props.show ? props.children :null}
    </>
}

export default IF;