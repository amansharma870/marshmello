import React from 'react'
import { StyleSheet, View,ToastAndroid } from 'react-native'
import { FloatingAction } from 'react-native-floating-action'
import { TextInput } from 'react-native-gesture-handler'
import ReactNativeModal from 'react-native-modal'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from '../Icon'
import { Ditem } from './diaryItem'

interface Props{
    isVisible:boolean,
    onDismiss:any;
    onSave:any;
    diaryItem ?:Ditem;
    onUpdate:any;
}


const CreateOrEditDiary=(props:Props)=>{
    const[TitleText,UpdateTitle]=React.useState('');
    const[Description,UpdateDis]=React.useState('');
    const actions = [
        {
          text: props.diaryItem ? "Update" : "Done",
          name: props.diaryItem ? "Update" : "bt_videocam",
          icon:(<Icon name={'checkmark'} color='#25D366'/>),
          position: 4
        }
      ];
      React.useEffect(()=>{
        if(props.diaryItem){
          UpdateDis(props.diaryItem.description)
          UpdateTitle(props.diaryItem.subject)
        }else{
          UpdateTitle('')
          UpdateDis('')
        }
      },[props.diaryItem])
      const onSaveorUpdate=()=>{
        if(TitleText === '' && Description ===''){
          return ToastAndroid.show('Please Fill Fields',ToastAndroid.LONG,)
        }
        const data = {
          id:props.diaryItem ? props.diaryItem.id : Math.random(),
          subject:TitleText,description:Description,}
        if(props.diaryItem){
          return props.onUpdate(data) 
        }
        return props.onSave(data);
      }
    return(
      <ReactNativeModal 
        style={{backgroundColor:'#fff',margin:0}}
        onDismiss={props.onDismiss}
        onBackButtonPress={props.onDismiss}
        isVisible={props.isVisible}
       >
      <View  style={styles.wrapper}>
        <TextInput onChangeText={val=>UpdateTitle(val)} value={TitleText} multiline={true} scrollEnabled={true} style={styles.inputesA} placeholder={'Enter Title'}/>
        <TextInput onChangeText={val=>UpdateDis(val)} value={Description} multiline={true} scrollEnabled={true} style={styles.inputesB} placeholder={'Enter Discription'}/>
      </View>
      <FloatingAction
                color='#3fbea5'
                actions={actions}
                onPressItem={onSaveorUpdate}
                
             />
     </ReactNativeModal>
    )
}

const styles = StyleSheet.create({
    inputesB:{
        borderBottomWidth:1,
        borderBottomColor:'#000',
        width:wp('90%'),
    },
    inputesA:{
      fontWeight:'bold',
      borderBottomColor:'#000',
      width:wp('90%'),
  },
    wrapper:{
        flex:1,
        alignItems:'center',
        justifyContent:'flex-start'
    }
})
CreateOrEditDiary.defaultProps={}
export default CreateOrEditDiary;