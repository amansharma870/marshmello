export interface Ditem{
    id:number,
    subject:string,
    description:string,
    date?:string,
    time?:string
}