import React from 'react'
import { View } from 'react-native'
import Container from '../components/Container';
import CustomText from '../components/CustomText';
import { Spacing } from '../styles/Global';

interface Props{
  route:any
}

interface State{

}

class ViewDiaryItem extends React.Component<Props,State>{
    constructor(props){
        super(props);
        this.state={};
    }
    render(){
        return(
            <Container>
                <CustomText containerStyles={{marginTop:Spacing.large.marginTop,flex:1,justifyContent:'flex-start'}}>
                   {this.props.route.params.diaryItem.subject}
                </CustomText>
                <CustomText>
                   {this.props.route.params.diaryItem.description}
                </CustomText>
            </Container>
        )
    }
    componentDidMount(){
    
    }
}

export default ViewDiaryItem;