import { Formik } from 'formik';
import React from 'react'
import { Alert, Dimensions, Platform, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import Container from '../components/Container'
import CustomButton from '../components/CustomButton'
import {Validators} from '../utils/Validators'
import {widthPercentageToDP as wp,heightPercentageToDP as hp,listenOrientationChange, removeOrientationListener} from 'react-native-responsive-screen'
import {utils} from '../utils/utils'
import IF from '../components/If'
import {lComponentStyle, pComponentStyle, Typography} from '../styles/Global'
import CustomeText from '../components/CustomText'
interface Props{

}
interface State{
    form:{
      EmailText:string,
      PasswordText:string
    }
    orientation:string
}

export class Login extends React.Component<Props,State>{
  private passwordRef;
  constructor(props){
    super(props);
    this.state={
        form:{
          EmailText:'',
          PasswordText:''
        },
        orientation:'portrait'
    }
  }
  
  onFire(){
    Alert.alert('WOw')
  }
  componentDidMount(){
    listenOrientationChange(this)
  }
  componentWillUnmount(){
    removeOrientationListener();
  }
  render(){
    console.log(this.state);
    const pStyles=portraitStyles();
    const lStyles=LandscapeStyles();
    return(
      <Container containerStyles={{alignItems:'center'}}>
        
        <CustomeText style={Typography.title}>
          Go!
        </CustomeText>
        <Formik 
          validateOnMount={true}
          validateOnChange={true}
          initialValues={this.state.form}
          onSubmit={()=>{}}
          validationSchema={Validators.loginValidators}
          >
          {(props)=>{
            return(
              <View style={{alignItems:'center'}}>
                   <TextInput 
                    onBlur={()=>{props.setFieldTouched('EmailText')}}
                    returnKeyType={'next'}
                    onSubmitEditing={() => this.passwordRef.focus()}
                    onChangeText={props.handleChange('EmailText')}
                    style={utils.dynamicStyle(pComponentStyle.textinput,lComponentStyle.textinput,this.state.orientation)}
                    placeholder={'Email'}
                    value={props.values.EmailText}
                  />
                  <IF show={props.dirty && props.touched.EmailText}>
                    <CustomeText style={Typography.errorText}>
                      {props.errors.EmailText}
                    </CustomeText>
                  </IF>
                  
                  <TextInput 
                    onSubmitEditing={()=>{
                      if(props.isValid){
                        console.log('Form is Valid')
                      }else{
                        console.log('Form Not Valid')
                      }
                    }}
                    
                    onBlur={()=>{props.setFieldTouched('PasswordText')}}
                    returnKeyType={'next'}
                    ref={ref => this.passwordRef = ref}
                    onChangeText={props.handleChange('PasswordText')}
                    style={utils.dynamicStyle(pComponentStyle.textinput,lComponentStyle.textinput,this.state.orientation)}
                    placeholder={'Password'}
                    value={props.values.PasswordText}
                    />
                    <IF show={props.dirty && props.touched.PasswordText}>
                      <CustomeText style={Typography.errorText}>
                        {props.errors.PasswordText}
                      </CustomeText>
                    </IF>
                  <CustomButton 
                      useIcon={true}
                      IconName={'checkmark'}
                      IconColor={'green'}
                      IconSize={21}
                      title={'Register'}
                      disabled={!props.isValid}
                      onPress={()=>{
                      if(props.isValid){
                        console.log('Form is Valid')
                      }else{
                        console.log('Form Not Valid',props.errors)
                      }
                    }}/>
                    
              </View>
             )
          }}
        </Formik>
       
      </Container>
    )
  }
}


const portraitStyles =()=>{
  return (
    StyleSheet.create({
     
      wrapper:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
      }
    })
  )
} 

const LandscapeStyles =()=>{
  return StyleSheet.create({
   
  }) 
} 

export default Login;