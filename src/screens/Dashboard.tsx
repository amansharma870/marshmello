import React from 'react'
import Container from '../components/Container'
import { StyleSheet, View,Text } from 'react-native'
import DiaryList from '../components/diaryList';
import {ActionSheetCustom as ActionSheet} from 'react-native-actionsheet'
import CustomText from '../components/CustomText';
import IF from '../components/If';
import { FloatingAction } from "react-native-floating-action";
import Icon from '../components/Icon'
import CreateDiary from '../components/modals/CreateDiary';
import { Ditem } from '../components/modals/diaryItem';
import {ScreenName} from '../utils/Navigations/Routes'
import Reactotron from 'reactotron-react-native'
interface Props{
   route:any;
   navigation:any
}
interface State{
    diaryItem:Ditem[]
    isModalVisible:boolean
    selectedItem:Ditem
}
enum ActionTypes{
   VIEW=0,
   Edit=1,
   Delete=2,
   Cancel=3
}
export class Dashboard extends React.Component<Props,State>{
   private actionSheetref;
    constructor(props){
      super(props);
      this.state = {
            diaryItem:[{
               id:0,
            date:'30-Jan-2021',
            time:'10:02 AM',
            subject:'A+ Secure',
            description:'Whatsapp is secure than other apps use this and make your experience with us.Whatsapp is secure than other apps use this and make your experience with us'
         },{
            id:1,
            date:'30-Jan-2021',
            time:'10:02 AM',
            subject:'B+ Secure',
            description:'Whatsapp is secure than other apps use this and make your experience with us.'
         },],
         isModalVisible:false,
         selectedItem:null
        };
    }
     actions = [
        {
          text: "Go Back",
          name: "bt_language",
          icon:(<Icon name={'chevron-forward'}/>),
          position: 1
        },
        {
          text: "Location",
          name: "bt_room",
          icon:(<Icon name={'location'}/>),
          position: 3
        },
        {
          text: "Add",
          name: "bt_videocam",
          icon:(<Icon name={'add'} color='#25D366'/>),
          position: 4 
        }
      ];
      addItems=val=>{
         const diaryItem=this.state.diaryItem;
         diaryItem.push(val);
         this.setState({
            diaryItem,
            isModalVisible:false
         })
      }
      updateItem = val => {
         const filteredItems = this.deleteAndGetItem(val);
         filteredItems.push(val);
         this.setState({
           diaryItem: filteredItems,
           isModalVisible: false,
           selectedItem: null,
         });
       };
      handleActionButton=index=>{
         switch(index){
            case ActionTypes.Cancel:{
               this.setState({selectedItem:null})
               break;
            }
            case ActionTypes.Edit:{
               this.setState({isModalVisible:true})
               break;
            }
            case ActionTypes.VIEW:{
                this.onView();
                break;
            }
             case ActionTypes.Delete:{
               this.onDelete();
             }
         }
      }
      onDelete(){
         const filteredItems = this.deleteAndGetItem(this.state.selectedItem);
    this.setState({diaryItem: filteredItems});
    console.log(filteredItems, 'filteredItems') 
      };
      deleteAndGetItem(item):Ditem[]{
         const diaryItems = this.state.diaryItem;
         const filteredItems= diaryItems.filter(data => data.id !== item.id);
         return filteredItems;
      }
      onView(){
         this.props.navigation.navigate(ScreenName.VIEW_DIARY_ITEM, {
            diaryItem: this.state.selectedItem,
          });
          this.setState({selectedItem: null});
      }
    render(){
        return(
           <Container containerStyles={{justifyContent:'flex-start'}}>
             <IF show={!!this.state.diaryItem.length}>
                <DiaryList onPress={(val)=>{
                   this.setState({selectedItem:val})
                   this.actionSheetref.show()}} diaryItem={this.state.diaryItem}/>
             </IF>
             <IF show={!this.state.diaryItem.length}>
                <CustomText style={styles.style}>
                    No items found, Please Add Items.
                </CustomText>
             </IF>
             <FloatingAction
                color='#3fbea5'
                actions={this.actions}
                onPressItem={
                   name=>{
                      this.setState({isModalVisible:true})
                   }
                }
             />
             <ActionSheet
                  ref={ref => this.actionSheetref = ref}
                  title={'Which one do you like ?'}
                  options={['View', 'Edit','Cancel', 'Delete']}
                  cancelButtonIndex={2}
                  destructiveButtonIndex={3}
                  onPress={this.handleActionButton}
               />
             <CreateDiary
             onUpdate={this.updateItem}
             diaryItem={this.state.selectedItem}
             onSave={this.addItems}
             isVisible={this.state.isModalVisible} 
             onDismiss={()=>{this.setState({isModalVisible:false,selectedItem:null})}}
             />
           </Container>
        )
    }
    componentDidMount(){

    }
}

const styles = StyleSheet.create({
    style:{
       width:'95%',
       alignItems:'center'
    },
})