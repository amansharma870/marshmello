import React from 'react'
import { Navigators } from './src/utils/Navigations/Navigator'
import Reactotron from 'reactotron-react-native'
import Store from './src/redux/actions/reducer/index'
import Provider from 'react-redux'
interface Props{}
interface State{}
export class App extends React.Component<Props,State>{
  constructor(props){
    super(props);
    this.configureReactotron();
  }
  configureReactotron(){
    Reactotron.clear();
      return(
        Reactotron.configure({
          host:'192.168.1.104',
          port:9090
        }).connect()
      )
  }
  render(){

   return(
        
          <Navigators />
        
    )
  }
}
export default App;

